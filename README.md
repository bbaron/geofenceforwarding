# Geofence Forwarding project #

This project contains the source code of the geofence forwarding project, including the GPSMapMatchingViewer project that leverages [OSRM](http://project-osrm.org/) to find the shortest route between two points.

## Project setup ##

### Dependencies ###

The dependencies for this project are the following: 
 - `proj4.9.1`
 - `geos3.4.2`
 - `gdal1.11.2_1`
 - `boost1.58.0`

### Qt installation ###

To setup this project, you need to have [Qt](https://www.qt.io/) installed on your computer. You can download the open source Qt version from https://www.qt.io/download and use the online installer to install the latest version of Qt (5.12.0 when writting this document) and Qt Creator, the Qt IDE to develop Qt applications.

### OSRM setup ###

[OSRM](http://project-osrm.org/) stands for the Open Source Routing Machine, which is an open-source project that is used by [Mapbox](https://blog.mapbox.com/mapbox-directions-powered-by-osrm-4-8-1-cf2c45ae9aa8) among others.

OSRM can be queried directly through an API with `curl` for instance or used directly as a library, `libosrm` via C++ interfaces. The related documentation can be found at http://project-osrm.org/docs/v5.15.2/api/#waypoint-object. An example can be found at https://github.com/Project-OSRM/osrm-backend/tree/master/example.

The instructions to build OSRM from source can be found at https://github.com/Project-OSRM/osrm-backend#building-from-source.
