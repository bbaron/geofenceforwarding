#ifndef TRACELOADER_H
#define TRACELOADER_H

#include <QString>
#include <QHash>
#include <QMap>
#include <QtConcurrent>

#include "tracegraphics.h"
#include "loader.h"

class TraceLoader: public Loader
{
public:
    TraceLoader(MainWindow* parent = 0, QString filename="", QString delimiter=",", bool header=false):
        Loader(parent, filename), _delimiter(delimiter), _header(header) { }

    QList<int> getNodes() {
        if(_loadResult)
            return _nodes.keys();
        return QList<int>();
    }

    QMap<uint, QPointF>* getNodeTrace(int nodeId) {
        if(_loadResult && _nodes.contains(nodeId)) {
            return _nodes.value(nodeId);
        } else {
            return NULL;
        }
    }

    void load();
    TraceGraphics *getTraceGraphics(int nodeId);
    void selectNode(TraceGraphics* nodeTrace);
    void unselectNode(TraceGraphics* nodeTrace);

private:
    bool concurrentLoad();

    QString _delimiter;
    bool _header;
    QMap<int, QMap<uint, QPointF>* > _nodes;
};

#endif // TRACELOADER_H
