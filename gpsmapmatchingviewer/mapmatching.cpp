#include "mapmatching.h"

#include "osrmwrapper.h"
#include "graphicsitems.h"

#include <QPen>

int MapMatching::computeMapMatching(int nodeId)
{
    _nodeId = nodeId;
    // Start concurrent thread and show progress bar
    _loadResult = QtConcurrent::run(this, &MapMatching::concurrentMapMatching, nodeId);
}

bool MapMatching::concurrentMapMatching(int nodeId)
{
    QMap<uint, QPointF>* trace = _traceLoader->getNodeTrace(nodeId);
    emit loadProgressChanged((qreal) 0.5);

    OSRMWrapper::getInstance().computeMapMatching(trace, &_subMapMatchings, _nodeId);

    qDebug() << "number of sub map matchings" << _subMapMatchings.size();

    emit loadProgressChanged((qreal) 1.0);
    return true;
}

QGraphicsItemGroup* MapMatching::draw()
{
    QGraphicsItemGroup* group = new QGraphicsItemGroup();
    group->setHandlesChildEvents(false);
    // draw the points and lines between them
    // (to show the succession of points)
    qDebug() << "Drawing map matching";
    QMap<uint, QPointF>* trace = _traceLoader->getNodeTrace(_nodeId);
    const SubMapMatching* curSub, *prevSub;
    int counter = 0;
    for(const SubMapMatching* sub: _subMapMatchings) {
        qDebug() << "sub matching #" << counter;
        // get the submapmatching
        group->addToGroup(sub->draw());
        connect(sub, &SubMapMatching::subMapMatchingSelected, [=](int subMapMatchingId, bool mod) {
            emit mapMatchingSelected(_nodeId, subMapMatchingId, mod);
        });

        if(counter == 0) {
            // define the first preSub map matching
            prevSub = sub;
        } else {
            // draw the lines between the adjacent sub map matchings prevSub and curSub
            curSub = sub;
            QPointF p1 = prevSub->getNodes().back()->getCenter();
            QPointF p2 = curSub->getNodes().front()->getCenter();
            ArrowLineItem* line12 = new ArrowLineItem(p1.x(), p1.y(), p2.x(), p2.y(), _nodeId, -1);
            line12->setColors(MATCH_LINK_JOINT_COL,MATCH_LINK_JOINT_COL,MATCH_LINK_JOINT_COL);
            line12->setWidths(TRACE_SELECTED_WID,TRACE_SELECTED_WID,TRACE_SELECTED_WID);

            // draw the corresponding line of the trace
            int idx3 = prevSub->getIndices().back();
            int idx4 = curSub->getIndices().front();
            QPointF p3 = trace->values().at(idx3);
            QPointF p4 = trace->values().at(idx4);

            ArrowLineItem* line34 = new ArrowLineItem(p3.x(), p3.y(), p4.x(), p4.y(), _nodeId, -1);
            line34->setColors(TRACE_LINK_JOINT_COL,TRACE_LINK_JOINT_COL,TRACE_LINK_JOINT_COL);
            line34->setWidths(TRACE_SELECTED_WID,TRACE_SELECTED_WID,TRACE_SELECTED_WID);

            group->addToGroup(line12);
            group->addToGroup(line34);
            prevSub = curSub;
        }
        counter++;
    }

    return group;
}

void MapMatching::setSelected(bool selected)
{
    for(int i = 0; i < _subMapMatchings.size(); ++i) {
        _subMapMatchings.at(i)->setSelected(selected);
    }
}

void MapMatching::setSelectedSubMapMatching(int subMapMatchingId, bool selected)
{
    if(subMapMatchingId >= 0 && subMapMatchingId < _subMapMatchings.size()) {
        _subMapMatchings.at(subMapMatchingId)->setSelected(selected);
    }
}
