#ifndef LOADER_H
#define LOADER_H

#include <QString>
#include <QtConcurrent>
#include <QString>
#include <QObject>

// forward declaration
class MainWindow;

class Loader: public QObject
{
    Q_OBJECT
public:
    Loader(MainWindow* parent = 0, QString filename = 0):
        _parent(parent), _filename(filename) {}

    void load();

signals:
    void loadProgressChanged(qreal);

protected:
    virtual bool concurrentLoad() = 0;

    QString _filename;
    QFuture<bool> _loadResult;
    MainWindow* _parent;
};

#endif // LOADER_H
