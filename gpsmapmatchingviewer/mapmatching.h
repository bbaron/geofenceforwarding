#ifndef MAPMATCHING_H
#define MAPMATCHING_H

#include <QtConcurrent>

#include "../library/osrm.hpp"
#include "../util/git_sha.hpp"
#include "../util/json_renderer.hpp"
#include "../util/routed_options.hpp"
#include "../util/simple_logger.hpp"

#include <osrm/json_container.hpp>
#include <osrm/libosrm_config.hpp>
#include <osrm/route_parameters.hpp>

#include "traceloader.h"

class SubMapMatching: public QObject {
    Q_OBJECT
public:
    SubMapMatching() {}
    void setSelected(bool selected) {
        for(int i = 0; i < _itemLineGraphics.size(); ++i) {
            _itemLineGraphics.at(i)->setTraceSelected(selected);
        }
        for(int i = 0; i < _itemWayPointGraphics.size(); ++i) {
            _itemWayPointGraphics.at(i)->setTraceSelected(selected);
        }
    }
    void addLine(ArrowLineItem* line) {
        _itemLineGraphics.append(line);
    }
    void addWayPoint(WaypointItem* wayPoint) {
        _itemWayPointGraphics.append(wayPoint);
    }
    void addIndice(int indice) {
        _indices.append(indice);
    }
    QList<WaypointItem*> getNodes() const { return _itemWayPointGraphics; }
    QList<int> getIndices() const { return _indices; }
    int getIndice(int idx) { return _indices.at(idx); }
    void setVisible(bool visible) {
        for(int i = 0; i < _itemLineGraphics.size(); ++i) {
            _itemLineGraphics.at(i)->setVisible(visible);
        }
        for(int i = 0; i < _itemWayPointGraphics.size(); ++i) {
            _itemWayPointGraphics.at(i)->setVisible(visible);
        }
    }
    QGraphicsItemGroup* draw() const {
        QGraphicsItemGroup* group = new QGraphicsItemGroup();
        group->setHandlesChildEvents(false);
        for(int i = 0; i < _itemLineGraphics.size(); ++i) {
            ArrowLineItem* line = _itemLineGraphics.at(i);
            connect(line, &ArrowLineItem::mousePressedEvent, [=](int subMapMatchingId, int linkId, bool mod) {
                emit subMapMatchingSelected(subMapMatchingId, mod);
            });
            group->addToGroup(line);
        }
        for(int i = 0; i < _itemWayPointGraphics.size(); ++i) {
            group->addToGroup(_itemWayPointGraphics.at(i));
        }

        return group;
    }

signals:
    void subMapMatchingSelected(int subMapMatchingId, bool mod) const;

private:
    QList<ArrowLineItem*> _itemLineGraphics;
    QList<WaypointItem*>  _itemWayPointGraphics;
    QList<int> _indices;
};


class MapMatching: public QObject
{
    Q_OBJECT
public:
    explicit MapMatching(TraceLoader* traceLoader):
        _traceLoader(traceLoader) { }

    int computeMapMatching(int nodeId);
    QGraphicsItemGroup* draw();

    void setVisible(bool visible) {
        for(int i = 0; i < _subMapMatchings.size(); ++i) {
            _subMapMatchings[i]->setVisible(visible);
        }
    }
    void setSelected(bool selected); // equivalent to setSelected
    void setSelectedSubMapMatching(int subMapMatchingId, bool selected); // equivalent to setSelectedLink

signals:
    void loadProgressChanged(qreal);
    void mapMatchingSelected(int nodeId, int subMapMatchingId, bool mod);

private:
    bool concurrentMapMatching(int nodeId);

    TraceLoader* _traceLoader;
    QList<SubMapMatching*> _subMapMatchings;
    QFuture<bool> _loadResult;
    int _nodeId;
};

#endif // MAPMATCHING_H
