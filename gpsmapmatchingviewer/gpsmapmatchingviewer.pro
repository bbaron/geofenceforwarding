#-------------------------------------------------
#
# Project created by QtCreator 2015-08-25T15:37:01
#
#-------------------------------------------------

QT       += core gui concurrent

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = gpsmapmatchingviewer
TEMPLATE = app

QMAKE_MAC_SDK = macosx10.11

CONFIG   += c++11

SOURCES += main.cpp\
        mainwindow.cpp \
    shapefileloader.cpp \
    traceloader.cpp \
    traceattaributes.cpp \
    mapmatching.cpp \
    route.cpp \
    osrmwrapper.cpp \
    traceexaminer.cpp \
    progressdialog.cpp

HEADERS  += mainwindow.h \
    customscene.h \
    shapefileloader.h \
    traceloader.h \
    graphicsview.h \
    progressdialog.h \
    traceattaributes.h \
    mapmatching.h \
    route.h \
    osrmwrapper.h \
    constants.h \
    graphicsitems.h \
    traceexaminer.h \
    tracegraphics.h \
    loader.h

FORMS    += mainwindow.ui \
    progressdialog.ui \
    traceattaributes.ui \
    traceexaminer.ui

QMAKE_CXXFLAGS = -mmacosx-version-min=10.8 -std=gnu0x -stdlib=libc+

unix|win32: LIBS += -L$$PWD/../../../../../../../usr/local/Cellar/proj/4.9.1/lib/ -lproj.9

INCLUDEPATH += $$PWD/../../../../../../../usr/local/Cellar/proj/4.9.1/include
DEPENDPATH += $$PWD/../../../../../../../usr/local/Cellar/proj/4.9.1/include


unix|win32: LIBS += -L$$PWD/../../../../../../../usr/local/Cellar/geos/3.4.2/lib/ -lgeos-3.4.2

INCLUDEPATH += $$PWD/../../../../../../../usr/local/Cellar/geos/3.4.2/include
DEPENDPATH += $$PWD/../../../../../../../usr/local/Cellar/geos/3.4.2/include


unix|win32: LIBS += -L$$PWD/../../../../../../../usr/local/Cellar/gdal/1.11.2_1/lib/ -lgdal.1

INCLUDEPATH += $$PWD/../../../../../../../usr/local/Cellar/gdal/1.11.2_1/include
DEPENDPATH += $$PWD/../../../../../../../usr/local/Cellar/gdal/1.11.2_1/include

unix: LIBS += -L$$PWD/../../../../../usr/local/Cellar/boost/1.58.0/lib/ -lboost_system

INCLUDEPATH += $$PWD/../../../../../usr/local/Cellar/boost/1.58.0/include
DEPENDPATH += $$PWD/../../../../../usr/local/Cellar/boost/1.58.0/include

unix: PRE_TARGETDEPS += $$PWD/../../../../../usr/local/Cellar/boost/1.58.0/lib/libboost_system.a


unix: LIBS += -L$$PWD/../../../../../usr/local/Cellar/boost/1.58.0/lib/ -lboost_filesystem

INCLUDEPATH += $$PWD/../../../../../usr/local/Cellar/boost/1.58.0/include
DEPENDPATH += $$PWD/../../../../../usr/local/Cellar/boost/1.58.0/include

unix: PRE_TARGETDEPS += $$PWD/../../../../../usr/local/Cellar/boost/1.58.0/lib/libboost_filesystem.a

unix: LIBS += -L$$PWD/../../../../../usr/local/Cellar/boost/1.58.0/lib/ -lboost_thread-mt

INCLUDEPATH += $$PWD/../../../../../usr/local/Cellar/boost/1.58.0/include
DEPENDPATH += $$PWD/../../../../../usr/local/Cellar/boost/1.58.0/include

unix: PRE_TARGETDEPS += $$PWD/../../../../../usr/local/Cellar/boost/1.58.0/lib/libboost_thread-mt.a

unix: LIBS += -L$$PWD/../../../../../usr/local/Cellar/boost/1.58.0/lib/ -lboost_program_options

INCLUDEPATH += $$PWD/../../../../../usr/local/Cellar/boost/1.58.0/include
DEPENDPATH += $$PWD/../../../../../usr/local/Cellar/boost/1.58.0/include

unix: PRE_TARGETDEPS += $$PWD/../../../../../usr/local/Cellar/boost/1.58.0/lib/libboost_program_options.a

unix: LIBS += -L$$PWD/../../osrm-backend/build/ -lOSRM

INCLUDEPATH += $$PWD/../../osrm-backend/include $$PWD/../../osrm-backend/third_party
DEPENDPATH += $$PWD/../../osrm-backend/include $$PWD/../../osrm-backend/third_party

unix: PRE_TARGETDEPS += $$PWD/../../osrm-backend/build/libOSRM.a

