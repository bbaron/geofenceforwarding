
import sys
import time
import schedule

def usage():
    return "Launch an API wrapper for the CTA or MARTA bus tramsit agencies\nUsage: %s {CTA|MARTA}" % sys.argv[0]

if(len(sys.argv) != 2):
    print usage()
    exit(1)

if(sys.argv[1] == 'CTA'):
    from CTA_api_wrapper import *
elif(sys.argv[1] == 'MARTA'):
    from MARTA_api_wrapper import *
else:
    print usage()
    exit(1)

if(len(sys.argv)) >= 3:
    OUTPUT_FILE = sys.argv[2]
else:
    OUTPUT_FILE = sys.argv[1] + "-out.txt"

def job():
    global vehicles, previous_time

    # update the time
    now = to_timestamp(get_time())
    print "last updated %ss" % (now - previous_time)
    previous_time = now

    active_vehicles = get_all_vehicles()

    nb_prev_active_veh = len(vehicles)
    nb_cur_active_veh  = len(active_vehicles)
    nb_added_veh = 0
    nb_updated_veh = 0
    nb_non_updated_veh = 0

    list_prev_active_veh = vehicles.keys()
    list_cur_active_veh  = active_vehicles.keys()
    list_added_veh = []

    # Set of unvisited vehicles to keep track
    unvisited_vehicles = set(vehicles.keys())

    # Open output file
    f = open(OUTPUT_FILE, "a")
    for (vid,veh) in active_vehicles.items():
        if vid not in unvisited_vehicles:
            # Add the vehicle
            vehicles[vid] = veh
            f.write(veh.to_file())
            nb_added_veh += 1
            list_added_veh.append(vid)
        else:
            # remove the vehicle from the unvisited set
            unvisited_vehicles.remove(vid)
            # compare the active vehicle with the previously recorded vehicle
            old_veh = vehicles[vid]
            if(old_veh.is_updated(veh, now)):
                # Update the vehicle
                del vehicles[vid]
                vehicles[vid] = veh
                f.write(veh.to_file())
                nb_updated_veh += 1
            else:
                nb_non_updated_veh += 1

    nb_del_veh = 0
    list_del_veh = []
    # Delete the vehicles that have not been visited for UNACTIVE_TIMEOUT time (not active anymore)
    for vid in unvisited_vehicles:
        if vehicles[vid].get_time_since_last_visit(now) > UNACTIVE_TIMEOUT:
            print vid, vehicles[vid].get_time_since_last_visit(now), UNACTIVE_TIMEOUT
            del vehicles[vid]
            nb_del_veh += 1
            list_del_veh.append(vid)

    f.close()
    # print stats
    if __debug__:
        print "[STATS] Old vehicles %s [%s]\n\tNew vehicles %s [%s]\n\tAdded vehicles %s [%s]\n\tDeleted vehicles %s [%s]\n\tUpdated vehicles %s\n\tNon updated vehicles %s" % (nb_prev_active_veh, ",".join(e for e in list_prev_active_veh), nb_cur_active_veh, ",".join(e for e in list_cur_active_veh), nb_added_veh,",".join(e for e in list_added_veh), nb_del_veh, ",".join(e for e in list_del_veh), nb_updated_veh, nb_non_updated_veh)


if __name__ == '__main__':
    previous_time = to_timestamp(get_time())
    vehicles = {}
    # erase contents of file
    open(OUTPUT_FILE, 'w').close()

    schedule.every(30).seconds.do(job)
    while 1:
        schedule.run_pending()
        time.sleep(1)
