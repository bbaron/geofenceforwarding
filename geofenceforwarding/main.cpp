#include "../library/osrm.hpp"
#include "../util/git_sha.hpp"
#include "../util/json_renderer.hpp"
#include "../util/routed_options.hpp"
#include "../util/simple_logger.hpp"

#include <osrm/json_container.hpp>
#include <osrm/libosrm_config.hpp>
#include <osrm/route_parameters.hpp>

#include <string>
#include <iomanip>

#include <QFile>
#include <QDebug>
#include <QHash>
#include <QPair>
#include <QMap>
#include <QStringList>
#include <QString>
#include <QDateTime>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

static inline void loadbar(unsigned int x, unsigned int n, unsigned int w = 50)
{
    if ( (x != n) && (x % (n/100+1) != 0) ) return;

    float ratio  =  x/(float)n;
    int   c      =  ratio * w;

    std::cout << std::setw(3) << (int)(ratio*100) << "% [";
    for (uint x=0; x<c; x++) std::cout << "=";
    for (uint x=c; x<w; x++) std::cout << " ";
    std::cout << "]\r" << std::flush;
}

int main(int argc, const char *argv[])
{
    LogPolicy::GetInstance().Unmute();

    std::string filename = "/Users/ben/Data/geofenceforwarding/data/siri.20130129.csv";
    SimpleLogger().Write() << "reading file " << filename;

    QHash<int, QMap<uint, boost::fusion::vector<double, double> >* > nodes;
    QFile * file = new QFile(QString::fromStdString(filename));
    if(!file->open(QFile::ReadOnly | QFile::Text))
    {
        return false;
    }
    while(!file->atEnd())
    {
        QString line = QString(file->readLine()).split(QRegExp("[\r\n]"), QString::SkipEmptyParts).at(0);
        QStringList fields = line.split(",");
        double lon = fields.at(8).toDouble();
        double lat = fields.at(9).toDouble();
        QDateTime timestamp = QDateTime::fromMSecsSinceEpoch(fields.at(0).toLongLong() / 1000);
        int nodeId = fields.at(12).toInt();
        // Create the node map if not already created
        if(!nodes.contains(nodeId)) {
            nodes.insert(nodeId, new QMap<uint, boost::fusion::vector<double, double> >());
        }
        // add the timestamp and corresponding position
        boost::fusion::vector<double, double> p(lat, lon);
        nodes.value(nodeId)->insert(timestamp.toTime_t(), p);

        // Indicate the load progress of the file
        double loadProgress = 100 - (int) (100 * file->bytesAvailable() / (qreal)file->size());
        loadbar(loadProgress, 100);
    }
    std::cout << std::endl;

    SimpleLogger().Write() << "total number of nodes " << nodes.size();

    int maxWayPoints = 0; // max = 3418
    for(auto it = nodes.begin(); it != nodes.end(); ++it) {
        if(it.value()->size() > maxWayPoints) {
            maxWayPoints = it.value()->size();
        }
    }

    SimpleLogger().Write() << "max waypoints number " << maxWayPoints;

    try
    {
        std::string ip_address;
        int ip_port, requested_thread_num, max_locations_map_matching;
        max_locations_map_matching = maxWayPoints;

        SimpleLogger().Write() << "max locations map matching " << max_locations_map_matching;
        bool trial_run = false;
        libosrm_config lib_config;
        const unsigned init_result = GenerateServerProgramOptions(
            argc, argv, lib_config.server_paths, ip_address, ip_port, requested_thread_num,
            lib_config.use_shared_memory, trial_run, lib_config.max_locations_distance_table,
            max_locations_map_matching);

        if (init_result == INIT_OK_DO_NOT_START_ENGINE)
        {
            return 0;
        }
        if (init_result == INIT_FAILED)
        {
            return 1;
        }
        SimpleLogger().Write() << "starting up engines, " << g_GIT_DESCRIPTION << ", max locations map matching " << max_locations_map_matching;

        OSRM routing_machine(lib_config);

        QHash<int, QMap<uint, boost::fusion::vector<double, double> >* > matched_nodes;
        for(auto it = nodes.begin(); it != nodes.end(); ++it) {
            int nodeId = it.key();
            SimpleLogger().Write() << "map matching for node #" << nodeId << " with " << it.value()->size() << " waypoints";
            RouteParameters route_parameters;
            route_parameters.zoom_level = 18;           // no generalization
            route_parameters.print_instructions = true; // turn by turn instructions
            route_parameters.alternate_route = true;    // get an alternate route, too
            route_parameters.geometry = true;           // retrieve geometry of route
            route_parameters.compression = false;        // polyline encoding
            route_parameters.check_sum = -1;            // see wiki
            route_parameters.service = "match";      // that's routing
            route_parameters.output_format = "json";
            route_parameters.jsonp_parameter = ""; // set for jsonp wrapping
            route_parameters.language = "";        // unused atm

            for(auto jt = it.value()->begin(); jt != it.value()->end(); ++jt) {
                uint ts = jt.key();
                const boost::fusion::vector<double, double> p = jt.value();
                route_parameters.addCoordinate(p);
                route_parameters.addTimestamp(ts);
            }

            osrm::json::Object json_result;
            std::vector<char> output;
            const int result_code = routing_machine.RunQuery(route_parameters, json_result);
            SimpleLogger().Write() << "http code: " << result_code;
            if(result_code == 200) {
                // if the matching worked for this trace
                osrm::json::render(output, json_result);
                std::string str_output(output.begin(),output.end());
                QJsonDocument d = QJsonDocument::fromJson(QString::fromStdString(str_output).toUtf8());
                QJsonObject matchingsObject = d.object();
                QJsonArray matchingsArray = matchingsObject["matchings"].toArray();
                int lastIndex = -1;
                foreach(const QJsonValue & value, matchingsArray) {
                    QJsonObject obj = value.toObject();
                    qDebug() << obj["indices"];
                    QJsonArray indices = obj["indices"].toArray();
                    if(lastIndex >= 0) {
                        // compare the gap timestamps
                        uint currentTimestamp = it.value()->keys().at(indices.first().toInt());
                        uint lastTimeStamp    = it.value()->keys().at(lastIndex);
                        SimpleLogger().Write() << "Difference: " << currentTimestamp - lastTimeStamp;
                    }
                    lastIndex = indices.last().toInt();
                }
            }
        }
    }
    catch (std::exception &current_exception)
    {
        SimpleLogger().Write(logWARNING) << "caught exception: " << current_exception.what();
        return -1;
    }
    return 0;
}
